# Rusty Drive

A Work in Progress Client to interact with Google Drive's API using Rust.


## Example

### Cargo.toml

Add **rusty-drive** as a dependency in your project's **Cargo.toml**.

```toml
[dependencies]
# This library
rusty-drive = { git = "https://gitlab.com/francopelusso/rusty-drive" }
# Your other dependencies
serde_json = "1.0"
```

### main.rs

Import whatever you need from **rusty-drive** crate and use it as you please.

```rust
// The private key you'll use to sign your JWT
const KEY: &str = "MY_KEY";

// It may also be an array containing bytes
// const KEY: &[u8] = b"MY_KEY";

// Or even the PKey<Private> from openssl::pkey module
// use openssl::pkey::{PKey, Private}
// const KEY: PKey<Private> = PKey::from_private_key_from_pem(b"MY_KEY");

use rusty_drive::jwt::JWTPKey;
use rusty_drive::google_drive::{GoogleDriveClient};
use serde_json;

fn main() {
    let mut client = GoogleDriveClient::from_pkey(JWTPKey::String(KEY));
    // Or from bytes' key:
    // let mut client = GoogleDriveClient::from_pkey(JWTPKey::Bytes(KEY));
    // Or from PKey<Private> key:
    // let mut client = GoogleDriveClient::from_pkey(JWTPKey::Key(KEY));

    // This will be empty ([]) as the client isn't authenticated
    println!("File List: {}", serde_json::to_string(&client.list_files()).unwrap());

    // Let's login with our service account and ask for full Drive's permissions
    client.authenticate(
        "drive-uploader-service-account@my-project.iam.gserviceaccount.com",
        vec!("https://www.googleapis.com/auth/drive"),
        None
    );

    println!("Access Token: {}", client.get_access_string());

    // This will print a list with all files contained within Service's Accounts root folder
    // It may be empty or not
    println!("File List: {}", serde_json::to_string(&client.list_files()).unwrap());
}

```
