use openssl::{pkey::{PKey, Private}, hash::MessageDigest, sign::Signer};
use serde::{Serialize, Deserialize};

#[allow(dead_code)]
pub enum JWTPKey {
    Key(PKey<Private>),
    String(&'static str),
    Bytes(&'static [u8])
}

#[derive(Serialize, Deserialize, Debug)]
pub struct JWTHeader {
    pub alg: String,
    pub typ: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct JWTClaimSet {
    pub iss: String,
    pub scope: String,
    pub aud: String,
    pub exp: u64,
    pub iat: u64,
}

pub struct JSONWebToken {
    pub header: JWTHeader,
    pub claim_set: JWTClaimSet,
    pub signature: String,
    pub token: String
}

pub struct JWTGenerator {
    pub pkey: PKey<Private>
}

impl JWTGenerator {
    pub fn generate(&self, header: JWTHeader, claim_set: JWTClaimSet) -> JSONWebToken {
        let serialized_header = serde_json::to_string(&header).unwrap();
        let serialized_claim_set = serde_json::to_string(&claim_set).unwrap();
        let enc_header = base64::encode(&serialized_header);
        let enc_claim_set = base64::encode(&serialized_claim_set);
        let signature = self.sign(&enc_header, &enc_claim_set);
        let token = format!("{}.{}.{}", enc_header, enc_claim_set, signature);
        JSONWebToken {
            header: header,
            claim_set: claim_set,
            signature: signature,
            token: token
        }
    }

    fn sign(&self, enc_header: &str, enc_claim_set: &str) -> String {
        let mut signer = self.signer();
        signer.update(format!("{}.{}", enc_header, enc_claim_set).as_bytes()).unwrap();
        let signed = signer.sign_to_vec().unwrap();
        base64::encode(&signed)
    }

    fn signer(&self) -> Signer<'_> {
        Signer::new(MessageDigest::sha256(), &self.pkey).unwrap()
    }
}
