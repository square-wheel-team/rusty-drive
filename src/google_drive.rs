use crate::jwt::{JSONWebToken, JWTClaimSet, JWTGenerator, JWTHeader, JWTPKey};
use openssl::pkey::PKey;
use reqwest::blocking::Client as BClient;
use reqwest::header::{AUTHORIZATION, HeaderMap};
use serde::{Serialize, Deserialize};
use std::time::SystemTime;

static DRIVE_SERVICE_ACCOUNT_AUD: &str = "https://oauth2.googleapis.com/token";
static DRIVE_SERVICE_ACCOUNT_GRANT_TYPE: (&str, &str) = (
    "grant_type",
    "urn:ietf:params:oauth:grant-type:jwt-bearer"
);
static DRIVE_BASE_URL: &str = "https://www.googleapis.com/drive/v3/";


pub struct GoogleDriveClient {
    pub authenticator: DriveServiceAccountAuthenticator,
    access: Option<DriveAccessToken>,
    req_client: BClient
}

pub struct DriveServiceAccountAuthenticator {
    jwt_generator: JWTGenerator,
}

#[allow(dead_code)]
struct DriveAccessToken {
    token: String,
    token_type: String,
    exp: u64,
    email: String,
    scopes: Vec<&'static str>
}

#[derive(Serialize, Deserialize)]
#[allow(non_snake_case)]
pub struct DriveFile {
    pub kind: String,
    pub id: String,
    pub name: String,
    pub mimeType: String
}

#[derive(Deserialize)]
struct OAuthResponse {
    access_token: String,
    token_type: String,
}

#[derive(Deserialize)]
#[allow(dead_code, non_snake_case)]
struct ListFilesResponse {
    incompleteSearch: bool,
    files: Vec<DriveFile>
}

#[allow(dead_code)]
impl GoogleDriveClient {
    pub fn from_pkey(pkey: JWTPKey) -> GoogleDriveClient {
        GoogleDriveClient{
            authenticator: DriveServiceAccountAuthenticator::new(pkey),
            access: None,
            req_client: BClient::new()
        }
    }

    pub fn authenticate(&mut self, email: &'static str, scopes: Vec<&'static str>,
                        exp_in: Option<u64>) {
        self.access = Some(self.authenticator.authenticate(
            email, scopes, exp_in, &self.req_client
        ));
    }

    pub fn is_authenticated(&self) -> bool {
        match &self.access {
            Some(access) => self.authenticator.get_current_timestamp() < access.exp,
            None => false
        }
    }

    pub fn get_access_string(&self) -> String {
        match &self.access {
            Some(access) => access.token.to_string(),
            None => String::new()
        }
    }

    pub fn list_files(&self) -> Vec<DriveFile> {
        match self.get_file_list() {
            Err(_) => vec!(),
            Ok(files) => files
        }
    }

    fn get_file_list(&self) -> Result<Vec<DriveFile>, &str> {
        if !self.is_authenticated() {
            Err("Client is not authenticated.")
        } else {
            // TODO: Maybe create a request method to set headers every time.
            let mut headers = HeaderMap::new();
            headers.insert(
                AUTHORIZATION,
                // TODO: Use access token's token_type
                format!("Bearer {}", self.get_access_string()).parse().unwrap()
            );
            let url = format!("{}{}", DRIVE_BASE_URL, "files");
            let file_list = self.req_client.get(&url)
                .headers(headers)
                .send().unwrap()
                .json::<ListFilesResponse>().unwrap();
            Ok(file_list.files)
        }
    }
}

impl DriveServiceAccountAuthenticator {
    pub fn new(pkey: JWTPKey,) -> DriveServiceAccountAuthenticator {
        let parsed_key = match pkey {
            JWTPKey::Key(key) => key,
            JWTPKey::String(key) => PKey::private_key_from_pem(key.as_bytes()).unwrap(),
            JWTPKey::Bytes(key) => PKey::private_key_from_pem(key).unwrap()
        };
        DriveServiceAccountAuthenticator{
            jwt_generator: JWTGenerator{pkey: parsed_key},
        }
    }

    pub fn get_jwt(&self, email: &str, scopes: Vec<&str>,
                   exp_in: Option<u64>) -> JSONWebToken {
        self.jwt_generator.generate(
            self.get_token_header(),
            self.get_token_claim_set(email, scopes, exp_in)
        )
    }

    fn authenticate(&self, email: &'static str, scopes: Vec<&'static str>,
                    exp_in: Option<u64>, req_client: &BClient) -> DriveAccessToken {
        let jwt = self.get_jwt(email, scopes.to_vec(), exp_in);
        let res = req_client.post(DRIVE_SERVICE_ACCOUNT_AUD)
            .form(&[DRIVE_SERVICE_ACCOUNT_GRANT_TYPE, ("assertion", &jwt.token)])
            .send().unwrap()
            .json::<OAuthResponse>().unwrap();
        DriveAccessToken{
            token: res.access_token,
            token_type: res.token_type,
            email: String::from(email),
            scopes: scopes,
            exp: jwt.claim_set.exp
        }
    }

    fn get_token_header(&self) -> JWTHeader {
        JWTHeader {
            alg: String::from("RS256"),
            typ: String::from("JWT")
        }
    }

    fn get_token_claim_set(&self, email: &str, scopes: Vec<&str>,
                           exp_in: Option<u64>) -> JWTClaimSet {
        let exp_secs = exp_in.unwrap_or(60);
        let iat = self.get_current_timestamp();
        let exp = iat + exp_secs;
        JWTClaimSet {
            iss: String::from(email),
            scope: scopes.join(","),
            aud: String::from(DRIVE_SERVICE_ACCOUNT_AUD),
            exp,
            iat
        }
    }

    fn get_current_timestamp(&self) -> u64 {
        match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(n) => n.as_secs(),
            Err(_) => panic!("SystemTime before UNIX_EPOCH")
        }
    }
}
